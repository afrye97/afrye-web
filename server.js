//Server.js file
//created by Allison Frye
//http://localhost:3000
var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var _dirname="/Users/allisonfrye/Desktop/website.first/"
var square = require('./square');
var wiki = require('./wiki.js');

const hostname = '127.0.0.1';
const port = 3000;

//exporting modules
app.get('/', function(req, res, next) {
  res.send('Hello World!'+ square.area(10));
});

//for different routes
app.use('/wiki', wiki);

//for static
app.use('/htmlfiles', express.static(_dirname))

//http://127.0.0.1:3000/htmlfiles/Website.html

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
